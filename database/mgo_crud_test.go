package database

import (
	"gopkg.in/mgo.v2/bson"
	"testing"
	"time"
)

type Doc struct {
	ID   bson.ObjectId `bson:"_id"`
	Name string        `bson:"name"`
	Age  int           `bson:"age"`
}

func TestMgoSession_Count(t *testing.T) {
	session, err := NewMgoSession(&DialInfo{
		Addrs:    []string{"192.168.100.107:57000", "192.168.100.10:57000", "192.168.100.36:57000"},
		Timeout:  10 * time.Second,
		Database: "admin",
		Username: "ryr-user",
		Password: "ryr.user.pwd.test",
	})
	if err != nil {
		t.Errorf("connect mgo %v", err)
	}
	params := &Params{
		DB:    "mgo-test",
		C:     "count",
		Query: map[string]interface{}{},
	}

	session.RemoveAll(params)

	n, err := session.Count(params)
	if err != nil {
		t.Errorf("mgo count %v", err)
	}
	if n != 0 {
		t.Errorf("mgo count %d != 0", n)
	}
}

func TestMgoSession_Find(t *testing.T) {
	session, err := NewMgoSession(&DialInfo{Addrs: []string{"localhost:27017"}})
	if err != nil {
		t.Errorf("connect mgo %v", err)
	}
	params := &Params{
		DB:    "mgo-test",
		C:     "find",
		Query: map[string]interface{}{},
		Options: map[string]interface{}{
			"sort":   []string{"-name"},
			"fields": bson.M{"name": 1, "age": 1},
		},
	}

	params.Docs = []interface{}{
		&Doc{Name: "li", Age: 27},
		&Doc{Name: "fei", Age: 28},
	}
	session.RemoveAll(params)
	session.Insert(params)

	data := []*Doc{
		{Name: "li", Age: 27},
		{Name: "fei", Age: 28},
	}
	var result []*Doc
	params.Result = &result
	err = session.Find(params)
	if err != nil {
		t.Errorf("mgo find %v", err)
	}
	if len(result) != len(data) {
		t.Errorf("mgo count %d != %d", len(result), len(data))
	}
	for i, datum := range data {
		if datum.Name != result[i].Name {
			t.Errorf("mgo find not match %s != %s", datum.Name, result[i].Name)
		}
		if datum.Age != result[i].Age {
			t.Errorf("mgo find not match %d != %d", datum.Age, result[i].Age)
		}
	}
}

func TestMgoSession_FindOne(t *testing.T) {
	session, err := NewMgoSession(&DialInfo{Addrs: []string{"localhost:27017"}})
	if err != nil {
		t.Errorf("connect mgo %v", err)
	}
	params := &Params{
		DB:    "mgo-test",
		C:     "find",
		Query: map[string]interface{}{"name": "fei"},
	}

	params.Docs = []interface{}{
		&Doc{Name: "fei", Age: 28},
	}
	session.RemoveAll(params)
	session.Insert(params)

	data := &Doc{Name: "fei", Age: 28}

	var result Doc
	params.Result = &result
	err = session.FindOne(params)
	if err != nil {
		t.Errorf("mgo find %v", err)
	}
	if data.Name != result.Name {
		t.Errorf("mgo find not match %s != %s", data.Name, result.Name)
	}
	if data.Age != result.Age {
		t.Errorf("mgo find not match %d != %d", data.Age, result.Age)
	}

}

func TestMgoSession_Insert(t *testing.T) {
	session, err := NewMgoSession(&DialInfo{
		Addrs:    []string{"192.168.100.107:57000", "192.168.100.10:57000", "192.168.100.36:57000"},
		Timeout:  10 * time.Second,
		Database: "admin",
		Username: "ryr-user",
		Password: "ryr.user.pwd.test",
	})
	if err != nil {
		t.Errorf("connect mgo %v", err)
	}
	params := &Params{
		DB:    "vox-ryr",
		C:     "insert",
		Query: map[string]interface{}{},
	}

	session.RemoveAll(params)

	docs := make([]interface{}, 0, 2)
	docs = append(docs, &Doc{Name: "fei", Age: 28, ID: bson.NewObjectId()})
	params.Docs = docs
	err = session.Insert(params)
	if err != nil {
		t.Errorf("mgo insert %v", err)
	}
	n, _ := session.Count(params)
	if n != 1 {
		t.Errorf("mgo insert not one")
	}
}

func TestMgoSession_Remove(t *testing.T) {
	session, err := NewMgoSession(&DialInfo{Addrs: []string{"localhost:27017"}})
	if err != nil {
		t.Errorf("connect mgo %v", err)
	}
	params := &Params{
		DB:    "mgo-test",
		C:     "insert",
		Query: map[string]interface{}{"name": "fei"},
	}

	n, _ := session.Count(params)
	if n <= 0 {
		t.Errorf("mgo insert has zero items")
	}
	err = session.Remove(params)
	if err != nil {
		t.Errorf("mgo remote items %v", err)
	}
	v, _ := session.Count(params)
	if v != 0 {
		t.Errorf("mgo note remove items")
	}
}

func TestMgoSession_RemoveAll(t *testing.T) {
	session, err := NewMgoSession(&DialInfo{Addrs: []string{"localhost:27017"}})
	if err != nil {
		t.Errorf("connect mgo %v", err)
	}
	params := &Params{
		DB:    "mgo-test",
		C:     "insert",
		Query: map[string]interface{}{},
	}
	err = session.RemoveAll(params)
	if err != nil {
		t.Errorf("mgo remove all %v", err)
	}
	nn, _ := session.Count(params)
	if nn != 0 {
		t.Errorf("mog not remove all, contains %d items", nn)
	}
}

func TestMgoSession_Update(t *testing.T) {
	session, err := NewMgoSession(&DialInfo{Addrs: []string{"localhost:27017"}})
	if err != nil {
		t.Errorf("connect mgo %v", err)
	}
	params := &Params{
		DB: "mgo-test",
		C:  "update",
	}
	params.Docs = []interface{}{&Doc{Name: "fei", Age: 27}}
	params.Query = map[string]interface{}{"name": "fei"}
	params.Update = map[string]interface{}{
		"$set": map[string]interface{}{"age": 28},
	}

	session.RemoveAll(params)

	var doc Doc
	params.Result = &doc
	session.Insert(params)
	err = session.Update(params)
	if err != nil {
		t.Errorf("mgo update %v", err)
	}
	err = session.FindOne(params)
	if err != nil {
		t.Errorf("mgo findOne %v", err)
	}
	if doc.Age != 28 {
		t.Errorf("mgo update error %d != 28", doc.Age)
	}

	session.RemoveAll(params)
}

func TestMgoSession_UpdateAll(t *testing.T) {
	session, err := NewMgoSession(&DialInfo{Addrs: []string{"localhost:27017"}})
	if err != nil {
		t.Errorf("connect mgo %v", err)
	}
	params := &Params{
		DB: "mgo-test",
		C:  "update",
	}
	params.Docs = []interface{}{
		&Doc{Name: "fei", Age: 27},
		&Doc{Name: "fei", Age: 28},
		&Doc{Name: "fei", Age: 29},
	}
	params.Query = map[string]interface{}{"name": "fei"}
	params.Update = map[string]interface{}{
		"$set": map[string]interface{}{"age": 30},
	}

	session.RemoveAll(params)

	var docs []*Doc
	params.Result = &docs
	session.Insert(params)
	err = session.UpdateAll(params)
	if err != nil {
		t.Errorf("mgo update %v", err)
	}
	err = session.Find(params)
	if err != nil {
		t.Errorf("mgo findOne %v", err)
	}
	for _, item := range docs {
		if item.Age != 30 {
			t.Errorf("mgo update all %d != 30", 30)
		}
	}
}
