package database

import (
	"crypto/tls"
	"net"
	"time"

	"bitbucket.org/ilifei/agostd/influxdb"
	"gopkg.in/redis.v5"
)

type ClusterOption struct {
	// A seed list of host:port addresses of cluster nodes.
	Addrs []string

	// The maximum number of retries before giving up. Command is retried
	// on network errors and MOVED/ASK redirects.
	// Default is 16.
	MaxRedirects int

	// Enables read queries for a connection to a Redis Cluster slave node.
	ReadOnly bool

	// Enables routing read-only queries to the closest master or slave node.
	RouteByLatency bool

	Password string

	DialTimeout  time.Duration
	ReadTimeout  time.Duration
	WriteTimeout time.Duration

	// PoolSize applies per cluster node and not for the whole cluster.
	PoolSize           int
	PoolTimeout        time.Duration
	IdleTimeout        time.Duration
	IdleCheckFrequency time.Duration
}

type Option struct {
	// The network type, either tcp or unix.
	// Default is tcp.
	Network string
	// host:port address.
	Addr string

	// Dialer creates new network connection and has priority over
	// Network and Addr options.
	Dialer func() (net.Conn, error)

	// Optional password. Must match the password specified in the
	// requirepass server configuration option.
	Password string
	// Database to be selected after connecting to the server.
	DB int

	// Maximum number of retries before giving up.
	// Default is to not retry failed commands.
	MaxRetries int

	// Dial timeout for establishing new connections.
	// Default is 5 seconds.
	DialTimeout time.Duration
	// Timeout for socket reads. If reached, commands will fail
	// with a timeout instead of blocking.
	// Default is 3 seconds.
	ReadTimeout time.Duration
	// Timeout for socket writes. If reached, commands will fail
	// with a timeout instead of blocking.
	// Default is 3 seconds.
	WriteTimeout time.Duration

	// Maximum number of socket connections.
	// Default is 10 connections.
	PoolSize int
	// Amount of time client waits for connection if all connections
	// are busy before returning an error.
	// Default is ReadTimeout + 1 second.
	PoolTimeout time.Duration
	// Amount of time after which client closes idle connections.
	// Should be less than server's timeout.
	// Default is to not close idle connections.
	IdleTimeout time.Duration
	// Frequency of idle checks.
	// Default is 1 minute.
	// When minus value is set, then idle check is disabled.
	IdleCheckFrequency time.Duration

	// Enables read only queries on slave nodes.
	ReadOnly bool

	// TLS Config to use. When set TLS will be negotiated.
	TLSConfig *tls.Config
}

// Cache is Redis cache adapter.
type Cache struct {
	clusterClient *redis.ClusterClient
	client        *redis.Client
	cluster       bool
}

func NewClusterCache(option *ClusterOption) (*Cache, error) {
	clusterOptions := &redis.ClusterOptions{
		Addrs:              option.Addrs,
		MaxRedirects:       option.MaxRedirects,
		ReadOnly:           option.ReadOnly,
		RouteByLatency:     option.RouteByLatency,
		Password:           option.Password,
		DialTimeout:        option.DialTimeout,
		ReadTimeout:        option.ReadTimeout,
		WriteTimeout:       option.WriteTimeout,
		PoolSize:           option.PoolSize,
		PoolTimeout:        option.PoolTimeout,
		IdleTimeout:        option.IdleTimeout,
		IdleCheckFrequency: option.IdleCheckFrequency,
	}
	rc := &Cache{
		clusterClient: redis.NewClusterClient(clusterOptions),
		cluster:       true,
	}
	return rc, rc.clusterClient.Ping().Err()
}

func NewCache(option *Option) (*Cache, error) {
	ops := &redis.Options{
		Addr: option.Addr,
	}
	rc := &Cache{
		client:  redis.NewClient(ops),
		cluster: false,
	}
	return rc, rc.client.Ping().Err()
}

// Keys
func (rc *Cache) Keys(pattern string) ([]string, error) {
	influxdb.CacheReceiver("keys")

	if rc.cluster {
		ret := make([]string, 0, 10)
		err := rc.clusterClient.ForEachNode(func(client *redis.Client) error {
			keys, err := client.Keys(pattern).Result()
			if err != nil {
				return err
			}
			ret = append(ret, keys...)
			return nil
		})
		if err != nil {
			return nil, err
		}
		return ret, nil
	}
	return rc.client.Keys(pattern).Result()
}

func (rc *Cache) Exists(key string) (bool, error) {
	influxdb.CacheReceiver("exists")

	if rc.cluster {
		return rc.clusterClient.Exists(key).Result()
	}
	return rc.client.Exists(key).Result()
}

func (rc *Cache) Get(key string) (string, error) {
	influxdb.CacheReceiver("get")

	if rc.cluster {
		return rc.clusterClient.Get(key).Result()
	}
	return rc.client.Get(key).Result()
}

func (rc *Cache) Set(key string, value interface{}, expiration time.Duration) (string, error) {
	influxdb.CacheReceiver("set")

	if rc.cluster {
		return rc.clusterClient.Set(key, value, expiration).Result()
	}
	return rc.client.Set(key, value, expiration).Result()
}

func (rc *Cache) Del(keys ...string) (int64, error) {
	influxdb.CacheReceiver("del")

	if rc.cluster {
		var err error
		var counter int64
		var n int64

		for _, key := range keys {
			n, err = rc.clusterClient.Del(key).Result()
			counter += n
		}

		return counter, err
	}
	return rc.client.Del(keys...).Result()
}

func (rc *Cache) Expire(key string, expiration time.Duration) (bool, error) {
	influxdb.CacheReceiver("expire")

	if rc.cluster {
		return rc.clusterClient.Expire(key, expiration).Result()
	}
	return rc.client.Expire(key, expiration).Result()
}

func (rc *Cache) ExpireAt(key string, tm time.Time) (bool, error) {
	influxdb.CacheReceiver("expireat")

	if rc.cluster {
		return rc.clusterClient.ExpireAt(key, tm).Result()
	}
	return rc.client.ExpireAt(key, tm).Result()
}

// Lists
func (rc *Cache) RPush(key string, values ...interface{}) (int64, error) {
	influxdb.CacheReceiver("rpush")

	if rc.cluster {
		return rc.clusterClient.RPush(key, values...).Result()
	}
	return rc.client.RPush(key, values...).Result()
}

func (rc *Cache) LLen(key string) (int64, error) {
	influxdb.CacheReceiver("llen")

	if rc.cluster {
		return rc.clusterClient.LLen(key).Result()
	}
	return rc.client.LLen(key).Result()
}

func (rc *Cache) LRange(key string, start, stop int64) ([]string, error) {
	influxdb.CacheReceiver("lrange")

	if rc.cluster {
		return rc.clusterClient.LRange(key, start, stop).Result()
	}
	return rc.client.LRange(key, start, stop).Result()
}

// Sorted Set
func (rc *Cache) ZRevRank(key, member string) (int64, error) {
	influxdb.CacheReceiver("zrevrank")

	if rc.cluster {
		return rc.clusterClient.ZRevRank(key, member).Result()
	}
	return rc.client.ZRevRank(key, member).Result()
}

func (rc *Cache) ZRevRange(key string, start, stop int64) ([]string, error) {
	influxdb.CacheReceiver("zrevrange")

	if rc.cluster {
		return rc.clusterClient.ZRevRange(key, start, stop).Result()
	}
	return rc.client.ZRevRange(key, start, stop).Result()
}

func (rc *Cache) ZScore(key, member string) (float64, error) {
	influxdb.CacheReceiver("zscore")

	if rc.cluster {
		return rc.clusterClient.ZScore(key, member).Result()
	}
	return rc.client.ZScore(key, member).Result()
}

func (rc *Cache) ZIncrBy(key string, increment float64, member string) (float64, error) {
	influxdb.CacheReceiver("zincrby")

	if rc.cluster {
		return rc.clusterClient.ZIncrBy(key, increment, member).Result()
	}
	return rc.client.ZIncrBy(key, increment, member).Result()
}

// Hash
func (rc *Cache) HIncrBy(key, field string, incr int64) (int64, error) {
	influxdb.CacheReceiver("hincrby")

	if rc.cluster {
		return rc.clusterClient.HIncrBy(key, field, incr).Result()
	}
	return rc.client.HIncrBy(key, field, incr).Result()
}

func (rc *Cache) HExists(key, field string) (bool, error) {
	influxdb.CacheReceiver("hexists")

	if rc.cluster {
		return rc.clusterClient.HExists(key, field).Result()
	}
	return rc.client.HExists(key, field).Result()
}

func (rc *Cache) HSet(key, field string, value interface{}) (bool, error) {
	influxdb.CacheReceiver("hset")

	if rc.cluster {
		return rc.clusterClient.HSet(key, field, value).Result()
	}
	return rc.client.HSet(key, field, value).Result()
}

func (rc *Cache) HGet(key, field string) (string, error) {
	influxdb.CacheReceiver("hget")

	if rc.cluster {
		return rc.clusterClient.HGet(key, field).Result()
	}
	return rc.client.HGet(key, field).Result()
}

func (rc *Cache) HGetAll(key string) (map[string]string, error) {
	influxdb.CacheReceiver("hgetall")

	if rc.cluster {
		return rc.clusterClient.HGetAll(key).Result()
	}
	return rc.client.HGetAll(key).Result()
}

func (rc *Cache) HLen(key string) (int64, error) {
	influxdb.CacheReceiver("hlen")

	if rc.cluster {
		return rc.clusterClient.HLen(key).Result()
	}
	return rc.client.HLen(key).Result()
}
