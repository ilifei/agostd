package database

import (
	"testing"
	"time"
)

func TestCache_Do_Get(t *testing.T) {
	cache, err := NewCache(&Option{
		Addr: "localhost:6379",
	})
	if err != nil {
		t.Errorf("conn redis %v", err)
	}
	cache.Set("hello", "world", time.Duration(0))
	s, err := cache.Get("hello")
	if err != nil {
		t.Errorf("redis GET %v", err)
	}
	if s != "world" {
		t.Errorf("redis GET %s != world", s)
	}

	cache.Del("hello")
}

func TestCache_Do_Set(t *testing.T) {
	cache, err := NewCache(&Option{
		Addr: "localhost:6379",
	})
	if err != nil {
		t.Errorf("conn redis %v", err)
	}

	cache.Del("hello")
	_, err = cache.Set("hello", "world", time.Duration(0))
	if err != nil {
		t.Errorf("redis SET %v", err)
	}

	ret, _ := cache.Get("hello")
	if ret != "world" {
		t.Errorf("redis GET %s != world", ret)
	}

	cache.Del("hello")
}

func TestCache_Do_Exists(t *testing.T) {

}

func TestCache_Do_Del(t *testing.T) {

}

func TestCache_Do_TTL(t *testing.T) {

}

func TestCache_Do_PTTL(t *testing.T) {

}

func TestCache_Do_HDel(t *testing.T) {

}

func TestCache_Do_HExists(t *testing.T) {

}

func TestCache_Do_HGet(t *testing.T) {

}
func TestCache_Do_HGetAll(t *testing.T) {

}
func TestCache_Do_HINCRBy(t *testing.T) {

}
func TestCache_Do_HKeys(t *testing.T) {

}
func TestCache_Do_HLen(t *testing.T) {

}
func TestCache_Do_HSet(t *testing.T) {

}
func TestCache_Do_BLPOP(t *testing.T) {

}
func TestCache_Do_BRPOP(t *testing.T) {

}
func TestCache_Do_LIndex(t *testing.T) {

}
func TestCache_Do_LInsert(t *testing.T) {

}
func TestCache_Do_LLen(t *testing.T) {

}
func TestCache_Do_LPop(t *testing.T) {

}
func TestCache_Do_LPush(t *testing.T) {

}
func TestCache_Do_LRange(t *testing.T) {

}
func TestCache_Do_LRem(t *testing.T) {

}

func TestCache_Do_LSet(t *testing.T) {

}
func TestCache_Do_RPop(t *testing.T) {

}
func TestCache_Do_RPush(t *testing.T) {

}
func TestCache_Do_RPopPush(t *testing.T) {

}
func TestCache_Do_ZAdd(t *testing.T) {

}
func TestCache_Do_ZCard(t *testing.T) {

}
func TestCache_Do_ZCount(t *testing.T) {

}
func TestCache_Do_ZINCRBy(t *testing.T) {

}
func TestCache_Do_ZInterStore(t *testing.T) {

}
func TestCache_Do_ZLexCount(t *testing.T) {

}
func TestCache_Do_ZRange(t *testing.T) {

}
func TestCache_Do_ZRank(t *testing.T) {

}
func TestCache_Do_ZRem(t *testing.T) {

}
func TestCache_Do_ZScore(t *testing.T) {

}
