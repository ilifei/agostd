package database

import (
	"time"

	"bitbucket.org/ilifei/agostd/influxdb"
	"gopkg.in/mgo.v2"
)

type DialInfo struct {
	Addrs          []string
	Timeout        time.Duration
	Database       string
	Username       string
	Password       string
	ReplicaSetName string
	Source         string
}

type MgoSession struct {
	s *mgo.Session
}

type Params struct {
	DB, C   string                 // 数据库名称，表名
	Docs    []interface{}          // 插入数据
	Query   interface{}            // 查询条件
	Options map[string]interface{} // 附加条件
	Update  interface{}            // 更新数据
	Result  interface{}            // 返回结果
}

func NewMgoSession(config *DialInfo) (*MgoSession, error) {
	dialInfo := &mgo.DialInfo{
		Addrs:     config.Addrs,
		FailFast:  true,
		PoolLimit: 5,
	}
	if config.Timeout != time.Duration(0) {
		dialInfo.Timeout = 10 * time.Second
	}
	if config.Database != "" {
		dialInfo.Database = config.Database
	}
	if config.Username != "" {
		dialInfo.Username = config.Username
	}
	if config.Password != "" {
		dialInfo.Password = config.Password
	}
	if config.ReplicaSetName != "" {
		dialInfo.ReplicaSetName = config.ReplicaSetName
	}
	if config.Source != "" {
		dialInfo.Source = config.Source
	}
	session, err := mgo.DialWithInfo(dialInfo)
	if err != nil {
		return nil, err
	}
	// 设置可以从“从库”读
	session.SetMode(mgo.Monotonic, true)
	return &MgoSession{s: session}, nil
}

func (m *MgoSession) Count(params *Params) (int, error) {
	influxdb.MongoReceiver("count")

	return m.s.DB(params.DB).C(params.C).Find(params.Query).Count()
}

func (m *MgoSession) Find(params *Params) error {
	influxdb.MongoReceiver("find")

	Q := m.s.DB(params.DB).C(params.C).Find(params.Query)
	if n, ok := params.Options["skip"]; ok {
		Q.Skip(n.(int))
	}
	if n, ok := params.Options["limit"]; ok {
		Q.Limit(n.(int))
	}
	if ss, ok := params.Options["sort"]; ok {
		Q.Sort(ss.([]string)...)
	}
	if fields, ok := params.Options["fields"]; ok {
		Q.Select(fields)
	}
	return Q.All(params.Result)
}

func (m *MgoSession) FindOne(params *Params) error {
	influxdb.MongoReceiver("findOne")

	Q := m.s.DB(params.DB).C(params.C).Find(params.Query)
	if n, ok := params.Options["skip"]; ok {
		Q.Skip(n.(int))
	}
	if n, ok := params.Options["limit"]; ok {
		Q.Limit(n.(int))
	}
	if ss, ok := params.Options["sort"]; ok {
		Q.Sort(ss.([]string)...)
	}
	if fields, ok := params.Options["fields"]; ok {
		Q.Select(fields)
	}
	return Q.One(params.Result)
}

func (m *MgoSession) Insert(params *Params) error {
	influxdb.MongoReceiver("insert")

	return m.s.DB(params.DB).C(params.C).Insert(params.Docs...)
}

func (m *MgoSession) Remove(params *Params) error {
	influxdb.MongoReceiver("remove")

	return m.s.DB(params.DB).C(params.C).Remove(params.Query)
}

func (m *MgoSession) RemoveAll(params *Params) error {
	influxdb.MongoReceiver("removeAll")

	_, err := m.s.DB(params.DB).C(params.C).RemoveAll(params.Query)
	return err
}

func (m *MgoSession) Update(params *Params) error {
	influxdb.MongoReceiver("update")

	return m.s.DB(params.DB).C(params.C).Update(params.Query, params.Update)
}

func (m *MgoSession) UpdateAll(params *Params) error {
	influxdb.MongoReceiver("updateAll")

	_, err := m.s.DB(params.DB).C(params.C).UpdateAll(params.Query, params.Update)
	return err
}
