package influxdb

import (
	"log"
	"os"
	"sync/atomic"
	"time"

	"github.com/influxdata/influxdb/client/v2"
)

// Mongo Counter
var (
	mgoCount  int64
	mgoCountX int64

	mgoFind  int64
	mgoFindX int64

	mgoFindOne  int64
	mgoFindOneX int64

	mgoInsert  int64
	mgoInsertX int64

	mgoRemove  int64
	mgoRemoveX int64

	mgoRemoveAll  int64
	mgoRemoveAllX int64

	mgoUpdate  int64
	mgoUpdateX int64

	mgoUpdateAll  int64
	mgoUpdateAllX int64
)

// Cache Counter
var (
	cacheKeys  int64
	cacheKeysX int64

	cacheExists  int64
	cacheExistsX int64

	cacheGet  int64
	cacheGetX int64

	cacheSet  int64
	cacheSetX int64

	cacheDel  int64
	cacheDelX int64

	cacheExpire  int64
	cacheExpireX int64

	cacheExpireAt  int64
	cacheExpireAtX int64

	cacheRPush  int64
	cacheRPushX int64

	cacheLLen  int64
	cacheLLenX int64

	cacheLRange  int64
	cacheLRangeX int64

	cacheZRevRank  int64
	cacheZRevRankX int64

	cacheZRevRange  int64
	cacheZRevRangeX int64

	cacheZScore  int64
	cacheZScoreX int64

	cacheZIncrBy  int64
	cacheZIncrByX int64

	cacheHIncrBy  int64
	cacheHIncrByX int64

	cacheHExists  int64
	cacheHExistsX int64

	cacheHSet  int64
	cacheHSetX int64

	cacheHGet  int64
	cacheHGetX int64

	cacheHGetAll  int64
	cacheHGetAllX int64

	cacheHLen  int64
	cacheHLenX int64
)

// API Counter
var (
	apiUsers  int64
	apiUsersX int64

	apiRanks  int64
	apiRanksX int64

	apiGates  int64
	apiGatesX int64

	apiGames  int64
	apiGamesX int64

	apiReplays  int64
	apiReplaysX int64

	apiAnswers  int64
	apiAnswersX int64

	apiReload  int64
	apiReloadX int64

	apiTips  int64
	apiTipsX int64

	apiPTips  int64
	apiPTipsX int64

	apiOrder  int64
	apiOrderX int64

	apiOrderProducts  int64
	apiOrderProductsX int64

	apiPoints  int64
	apiPointsX int64
)

var host string
var config client.UDPConfig
var c client.Client

func init() {
	var err error

	host, _ = os.Hostname()

	// 初始化连接
	config = client.UDPConfig{Addr: "10.0.1.224:8089"}
	c, err = client.NewUDPClient(config)
	if err != nil {
		log.Println("Error: ", err.Error())
	}

	go StartBeatPoints()
}

func StartBeatPoints() {
	// 初始化计数器
	c := time.Tick(1 * time.Second)
	for range c {
		batchCommit()
	}
}

func APIReceiver(key string) {
	switch key {
	case "users":
		atomic.AddInt64(&apiUsers, 1)
	case "ranks":
		atomic.AddInt64(&apiRanks, 1)
	case "gates":
		atomic.AddInt64(&apiGates, 1)
	case "games":
		atomic.AddInt64(&apiGames, 1)
	case "replays":
		atomic.AddInt64(&apiReplays, 1)
	case "answers":
		atomic.AddInt64(&apiAnswers, 1)
	case "reload":
		atomic.AddInt64(&apiReload, 1)
	case "tips":
		atomic.AddInt64(&apiTips, 1)
	case "ptips":
		atomic.AddInt64(&apiPTips, 1)
	case "order":
		atomic.AddInt64(&apiOrder, 1)
	case "orderProducts":
		atomic.AddInt64(&apiOrderProducts, 1)
	case "points":
		atomic.AddInt64(&apiPoints, 1)
	}
}

func CacheReceiver(key string) {
	switch key {
	case "keys":
		atomic.AddInt64(&cacheKeys, 1)
	case "exists":
		atomic.AddInt64(&cacheExists, 1)
	case "get":
		atomic.AddInt64(&cacheGet, 1)
	case "set":
		atomic.AddInt64(&cacheSet, 1)
	case "del":
		atomic.AddInt64(&cacheDel, 1)
	case "expire":
		atomic.AddInt64(&cacheExpire, 1)
	case "expireat":
		atomic.AddInt64(&cacheExpireAt, 1)
	case "rpush":
		atomic.AddInt64(&cacheRPush, 1)
	case "llen":
		atomic.AddInt64(&cacheLLen, 1)
	case "lrange":
		atomic.AddInt64(&cacheLRange, 1)
	case "zrevrank":
		atomic.AddInt64(&cacheZRevRank, 1)
	case "zrevrange":
		atomic.AddInt64(&cacheZRevRange, 1)
	case "zscore":
		atomic.AddInt64(&cacheZScore, 1)
	case "zincrby":
		atomic.AddInt64(&cacheZIncrBy, 1)
	case "hincrby":
		atomic.AddInt64(&cacheHIncrBy, 1)
	case "hexists":
		atomic.AddInt64(&cacheHExists, 1)
	case "hset":
		atomic.AddInt64(&cacheHSet, 1)
	case "hget":
		atomic.AddInt64(&cacheHGet, 1)
	case "hgetall":
		atomic.AddInt64(&cacheHGetAll, 1)
	case "hlen":
		atomic.AddInt64(&cacheHLen, 1)
	}
}

func MongoReceiver(key string) {
	switch key {
	case "count":
		atomic.AddInt64(&mgoCount, 1)
	case "find":
		atomic.AddInt64(&mgoFind, 1)
	case "findOne":
		atomic.AddInt64(&mgoFindOne, 1)
	case "insert":
		atomic.AddInt64(&mgoInsert, 1)
	case "remove":
		atomic.AddInt64(&mgoRemove, 1)
	case "removeAll":
		atomic.AddInt64(&mgoRemoveAll, 1)
	case "update":
		atomic.AddInt64(&mgoUpdate, 1)
	case "updateAll":
		atomic.AddInt64(&mgoUpdateAll, 1)
	}
}

func batchCommit() {
	swapCounter()

	bp, _ := client.NewBatchPoints(client.BatchPointsConfig{})

	// mgo point
	mgoPoint, err := newMgoPoint()
	if err != nil {
		log.Printf("mgo point: %v", err)
	} else {
		bp.AddPoint(mgoPoint)
	}

	// cache point
	cachePoint, err := newCachePoint()
	if err != nil {
		log.Printf("cache point: %v", err)
	} else {
		bp.AddPoint(cachePoint)
	}

	// api point
	apiPoint, err := newAPIPoint()
	if err != nil {
		log.Printf("api point: %v", err)
	} else {
		bp.AddPoint(apiPoint)
	}

	c.Write(bp)
	cleanCounter()
}

func newMgoPoint() (*client.Point, error) {
	tags := map[string]string{
		"host": host,
	}
	sum := mgoCountX + mgoFindX + mgoFindOneX + mgoInsertX + mgoRemoveX + mgoRemoveAllX + mgoUpdateX + mgoUpdateAllX
	fields := map[string]interface{}{
		"count":     mgoCountX,
		"find":      mgoFindX,
		"findOne":   mgoFindOneX,
		"insert":    mgoInsertX,
		"remove":    mgoRemoveX,
		"removeAll": mgoRemoveAllX,
		"update":    mgoUpdateX,
		"updateAll": mgoUpdateAllX,

		"sum": sum,
	}
	return client.NewPoint("ago_sudoku_mongo", tags, fields)
}

func newCachePoint() (*client.Point, error) {
	tags := map[string]string{
		"host": host,
	}
	sum := cacheKeysX + cacheExistsX + cacheGetX + cacheSetX + cacheDelX + cacheExpireX + cacheExpireAtX + cacheRPushX + cacheLLenX + cacheLRangeX + cacheZRevRankX + cacheZRevRangeX + cacheZScoreX + cacheZIncrByX + cacheHIncrByX + cacheHExistsX + cacheHSetX + cacheHGetX + cacheHGetAllX + cacheHLenX
	fields := map[string]interface{}{
		"keys":      cacheKeysX,
		"exists":    cacheExistsX,
		"get":       cacheGetX,
		"set":       cacheSetX,
		"del":       cacheDelX,
		"expire":    cacheExpireX,
		"expireat":  cacheExpireAtX,
		"rpush":     cacheRPushX,
		"llen":      cacheLLenX,
		"lrange":    cacheLRangeX,
		"zrevrank":  cacheZRevRankX,
		"zrevrange": cacheZRevRangeX,
		"zscore":    cacheZScoreX,
		"zincrby":   cacheZIncrByX,
		"hincrby":   cacheHIncrByX,
		"hexists":   cacheHExistsX,
		"hset":      cacheHSetX,
		"hget":      cacheHGetX,
		"hgetall":   cacheHGetAllX,
		"hlen":      cacheHLenX,

		"sum": sum,
	}
	return client.NewPoint("ago_sudoku_cache", tags, fields)
}

func newAPIPoint() (*client.Point, error) {
	tags := map[string]string{
		"host": host,
	}
	sum := apiUsersX + apiRanksX + apiGatesX + apiGamesX + apiReplaysX + apiAnswersX + apiReloadX + apiTipsX + apiPTipsX + apiOrderX + apiOrderProductsX + apiPointsX
	fields := map[string]interface{}{
		"users":         apiUsersX,
		"ranks":         apiRanksX,
		"gates":         apiGatesX,
		"games":         apiGamesX,
		"replays":       apiReplaysX,
		"answers":       apiAnswersX,
		"reload":        apiReloadX,
		"tips":          apiTipsX,
		"ptips":         apiPTipsX,
		"order":         apiOrderX,
		"orderProducts": apiOrderProductsX,
		"points":        apiPointsX,

		"sum": sum,
	}
	return client.NewPoint("ago_sudoku_api", tags, fields)
}

// 交换计数器
func swapCounter() {
	// mongo swap
	mgoCountX = atomic.SwapInt64(&mgoCount, mgoCountX)
	mgoFindX = atomic.SwapInt64(&mgoFind, mgoFindX)
	mgoFindOneX = atomic.SwapInt64(&mgoFindOne, mgoFindOneX)
	mgoInsertX = atomic.SwapInt64(&mgoInsert, mgoInsertX)
	mgoRemoveX = atomic.SwapInt64(&mgoRemove, mgoRemoveX)
	mgoRemoveAllX = atomic.SwapInt64(&mgoRemoveAll, mgoRemoveAllX)
	mgoUpdateX = atomic.SwapInt64(&mgoUpdate, mgoUpdateX)
	mgoUpdateAllX = atomic.SwapInt64(&mgoUpdateAll, mgoUpdateAllX)

	// cache swap
	cacheKeysX = atomic.SwapInt64(&cacheKeys, cacheKeysX)
	cacheExistsX = atomic.SwapInt64(&cacheExists, cacheExistsX)
	cacheGetX = atomic.SwapInt64(&cacheGet, cacheGetX)
	cacheSetX = atomic.SwapInt64(&cacheSet, cacheSetX)
	cacheDelX = atomic.SwapInt64(&cacheDel, cacheDelX)
	cacheExpireX = atomic.SwapInt64(&cacheExpire, cacheExpireX)
	cacheExpireAtX = atomic.SwapInt64(&cacheExpireAt, cacheExpireAtX)
	cacheRPushX = atomic.SwapInt64(&cacheRPush, cacheRPushX)
	cacheLLenX = atomic.SwapInt64(&cacheLLen, cacheLLenX)
	cacheLRangeX = atomic.SwapInt64(&cacheLRange, cacheLRangeX)
	cacheZRevRankX = atomic.SwapInt64(&cacheZRevRank, cacheZRevRankX)
	cacheZRevRangeX = atomic.SwapInt64(&cacheZRevRange, cacheZRevRangeX)
	cacheZScoreX = atomic.SwapInt64(&cacheZScore, cacheZScoreX)
	cacheZIncrByX = atomic.SwapInt64(&cacheZIncrBy, cacheZIncrByX)
	cacheHIncrByX = atomic.SwapInt64(&cacheHIncrBy, cacheHIncrByX)
	cacheHExistsX = atomic.SwapInt64(&cacheHExists, cacheHExistsX)
	cacheHSetX = atomic.SwapInt64(&cacheHSet, cacheHSetX)
	cacheHGetX = atomic.SwapInt64(&cacheHGet, cacheHGetX)
	cacheHGetAllX = atomic.SwapInt64(&cacheHGetAll, cacheHGetAllX)
	cacheHLenX = atomic.SwapInt64(&cacheHLen, cacheHLenX)

	// api swap
	apiUsersX = atomic.SwapInt64(&apiUsers, apiUsersX)
	apiRanksX = atomic.SwapInt64(&apiRanks, apiRanksX)
	apiGatesX = atomic.SwapInt64(&apiGates, apiGatesX)
	apiGamesX = atomic.SwapInt64(&apiGames, apiGamesX)
	apiReplaysX = atomic.SwapInt64(&apiReplays, apiReplaysX)
	apiAnswersX = atomic.SwapInt64(&apiAnswers, apiAnswersX)
	apiReloadX = atomic.SwapInt64(&apiReload, apiReloadX)
	apiTipsX = atomic.SwapInt64(&apiTips, apiTipsX)
	apiPTipsX = atomic.SwapInt64(&apiPTips, apiPTipsX)
	apiOrderX = atomic.SwapInt64(&apiOrder, apiOrderX)
	apiOrderProductsX = atomic.SwapInt64(&apiOrderProducts, apiOrderProductsX)
	apiPointsX = atomic.SwapInt64(&apiPoints, apiPointsX)
}

// 清空赋值计数器
func cleanCounter() {
	// mongo clean
	atomic.SwapInt64(&mgoCountX, 0)
	atomic.SwapInt64(&mgoFindX, 0)
	atomic.SwapInt64(&mgoFindOneX, 0)
	atomic.SwapInt64(&mgoInsertX, 0)
	atomic.SwapInt64(&mgoRemoveX, 0)
	atomic.SwapInt64(&mgoRemoveAllX, 0)
	atomic.SwapInt64(&mgoUpdateX, 0)
	atomic.SwapInt64(&mgoUpdateAllX, 0)

	// cache clean
	atomic.SwapInt64(&cacheKeysX, 0)
	atomic.SwapInt64(&cacheExistsX, 0)
	atomic.SwapInt64(&cacheGetX, 0)
	atomic.SwapInt64(&cacheSetX, 0)
	atomic.SwapInt64(&cacheDelX, 0)
	atomic.SwapInt64(&cacheExpireX, 0)
	atomic.SwapInt64(&cacheExpireAtX, 0)
	atomic.SwapInt64(&cacheRPushX, 0)
	atomic.SwapInt64(&cacheLLenX, 0)
	atomic.SwapInt64(&cacheLRangeX, 0)
	atomic.SwapInt64(&cacheZRevRankX, 0)
	atomic.SwapInt64(&cacheZRevRangeX, 0)
	atomic.SwapInt64(&cacheZScoreX, 0)
	atomic.SwapInt64(&cacheZIncrByX, 0)
	atomic.SwapInt64(&cacheHIncrByX, 0)
	atomic.SwapInt64(&cacheHExistsX, 0)
	atomic.SwapInt64(&cacheHSetX, 0)
	atomic.SwapInt64(&cacheHGetX, 0)
	atomic.SwapInt64(&cacheHGetAllX, 0)
	atomic.SwapInt64(&cacheHLenX, 0)

	// api clean
	atomic.SwapInt64(&apiUsersX, 0)
	atomic.SwapInt64(&apiRanksX, 0)
	atomic.SwapInt64(&apiGatesX, 0)
	atomic.SwapInt64(&apiGamesX, 0)
	atomic.SwapInt64(&apiReplaysX, 0)
	atomic.SwapInt64(&apiAnswersX, 0)
	atomic.SwapInt64(&apiReloadX, 0)
	atomic.SwapInt64(&apiTipsX, 0)
	atomic.SwapInt64(&apiPTipsX, 0)
	atomic.SwapInt64(&apiOrderX, 0)
	atomic.SwapInt64(&apiOrderProductsX, 0)
	atomic.SwapInt64(&apiPointsX, 0)
}
